""" Task 1D demonstration program """

from floodsystem import geo

stations = build_station_list()
print(rivers_with_station(stations)[:10])

dictionary = stations_by_river(stations)
print("\n", dictionary['River Aire'])
print("\n", dictionary['River Cam'])
print("\n", dictionary['Thames'])