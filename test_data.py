# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 15:05:38 2017

"""
import pytest

from floodsystem.stationdata import build_station_list

from floodsystem.geo import stations_by_distance


def test_1B():
    stations = build_station_list()
    sta = []
    for station in stations:
        if station.name == 'Cam':
            sta.append(station)
        if station.name == 'Girton':
            sta.append(station)
        
    p =  (52.2053, 0.1218)
    m = stations_by_distance(sta, p)
    
    assert m[0][1]>0
    assert m[1][1]>0