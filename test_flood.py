# Test file for flood.py

import pytest
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

def test_stations_level_over_threshold():
    """Test the flood function to see if they return a river"""
    stations = build_station_list()
    update_water_levels(stations)
    all_stations = stations_level_over_threshold(stations, 0)
    assert len(all_stations) > 0