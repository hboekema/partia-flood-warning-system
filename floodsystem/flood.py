""" This module provides a flood warning based on the current water level of
each monitoring station in relation to its typical range """

from floodsystem.utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    """ Function that returns a list of stations with a relative water level
    over a user-specified tolerance """
    stations_over_tol = []
    for station in stations:
        # If the water level is over the tolerance (and the data is consistent)...
        if station.typical_range_consistent() and type(station.relative_water_level()) != type(None) and station.relative_water_level() > tol:
            # ...add this station and its level to the list
            stations_over_tol.append((station, station.relative_water_level()))
    # Sort the list by the relative water levels and return
    return sorted_by_key(stations_over_tol, 1, True)
    
def stations_highest_rel_level(stations, N):
    """ Function that returns a list of (station, rel level) tuples for the 10 
    stations with the highest relative water level """
    return stations_level_over_threshold(stations, float('-inf'))[:N]