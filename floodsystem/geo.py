""" This module contains a collection of functions related to
geographical data. """

from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import build_station_list

def distance_between_stations(coords_1, coords_2):
    """ Function that finds and returns the distance between two stations
    (whose coordinates are supplied as arguments), in km """
    from math import radians, sin, cos, asin, sqrt
    
    # Earth's average radius, in km
    RADIUS = 6371
    
    # unpack the coordinates
    lat1, lng1 = coords_1
    lat2, lng2 = coords_2

    # convert all latitudes/longitudes from degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate the distance between the points
    lat_diff = lat2 - lat1
    lng_diff = lng2 - lng1
    distance = sin(lat_diff * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng_diff * 0.5) ** 2
    return 2 * RADIUS * asin(sqrt(distance)) # distance in km

def stations_by_distance(stations, p):
    """ Returns a list of stations and their distance from a point p """
    sta_req = [(station.name, station.coord) for station in stations]
    sta_dist = [(name, distance_between_stations(p, coords)) for name, coords in sta_req]
    
    return sorted_by_key(sta_dist, 1)

def stations_within_radius(stations, centre, r):
    """ Function that returns a list of MonitoringStation objects within a 
    radius r of a centre """
    meet_criterium = []
    for station in stations:
        # Check to see if the distance between the stations is smaller than r
        if distance_between_stations(station.coord, centre) <= r:
            # If so, save this station
            meet_criterium.append(station)
    
    # Return a list of the stations that match this condition 
    return sorted(meet_criterium)

def rivers_with_station(stations):
    """ Function that, given a list of stations, returns a set of the rivers
    on which these stations lie """
    # Create set of rivers by saving each 'river' data member
    rivers = {station.river for station in stations}
    # Sort and return this set
    return sorted(rivers)
    
def stations_by_river(stations):
    """ Function that returns the number of stations on each river """
    # Create set of rivers
    rivers = rivers_with_station(stations)
    river_station_dict = {}

    # Use nested for loops to test whether the station lies on the river
    for river in rivers:
        station_list = []
        for station in stations:
            if river == station.river:
                # if this is the case, add it to a list
                station_list.append(station.name)
        
        # this list is then saved as the map (the river is the key)
        river_station_dict[river] = sorted(station_list)
        
    return river_station_dict
    
def rivers_by_station_number(stations, N):
    """ Function that returns a list of tuples of the N rivers with the 
    most stations and the number of stations on that river """
    rivers = rivers_with_station(stations)
    # Check that there are at least N rivers and that N is not less than 0
    if N > len(rivers):
        raise ValueError("N cannot be larger than the number of rivers (", rivers.size(), ")")
    elif N < 0:
        raise ValueError("N must be a positive number")
        
    # List comprehension to create a list of (river, number of stations) tuples 
    river_dict = stations_by_river(stations)    
    river_stations = [(k, len(river_dict[k])) for k in river_dict]
    # Sort this list in order of descending number of stations
    river_stations = sorted_by_key(river_stations, 1, True)
    
    # Ensure that on the (outside) 'edge' of the list with the same number of stations are included
    return_list = []
    n = 0
    while n < N or river_stations[n-1][1] == river_stations[n][1]:
        return_list.append(river_stations[n])
        n += 1
        
    return return_list
    
        
        
        