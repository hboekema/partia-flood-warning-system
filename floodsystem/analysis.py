""" Module that analyses """

import numpy as np
from matplotlib.dates import date2num
from matplotlib import pyplot as plt

def polyfit(dates, levels, p):
    
    if len(dates) == 0 or len(levels) == 0:
        pass
    else:
        # Using shifted dates, find coefficients of best-fit polynomial f(x) of degree 4   
        p_coeff = np.polyfit(dates, levels, p)

        # Convert coefficient into a polynomial that can be evaluated,
        # e.g. poly(0.3)
        poly = np.poly1d(p_coeff)

    try:
        return poly
    except:
        pass