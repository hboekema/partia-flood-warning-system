""" This module contains functions related to plotting station data"""

def plot_water_levels(station, dates, levels):
    
    import matplotlib.pyplot as plt
    import datetime
    
    high_range = []
    low_range = []    

    for date in dates:
        high_range.append(station.typical_range[0])
        low_range.append(station.typical_range[1])
    
    # plot levels against dates and relative levels
    plt.plot(dates, levels) 
    plt.plot(dates, high_range)
    plt.plot(dates, low_range)
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    plt.xlim(dates[-1], dates[0])
    

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p, n):
    """ Plots water levels with typical high/low ranges and a polynomial prediction curve of degree p """
    
    dates_float = matplotlib.dates.date2num(dates)
    
    if len(dates_float) == 0 or len(levels) == 0:
        pass
    else:
        high_range = []
        low_range = []    

        for date in dates_float:
            high_range.append(station.typical_range[0])
            low_range.append(station.typical_range[1])
        
        poly = polyfit(dates_float, levels, p)
    
        # Plot original data points and high/low range
        plt.plot(dates, levels, '.')
        plt.plot(dates, high_range)
        plt.plot(dates, low_range)

        # Plot polynomial fit at 30 points along interval
        x = np.linspace(dates_float[-1], dates_float[0], 30)
        plt.plot(x, poly(x))
        
        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('datetime')
        plt.ylabel('water level (m)')
        plt.xticks(rotation=45);
        plt.title(station.name)
        plt.xlim(dates[-1], dates[0])

        # Display plot
        plt.tight_layout()  # This makes sure plot does not cut off date labels
        plt.show()
        
        grad_polynomial = []
        

        if n == True:
            for i in range(10):
                grad_polynomial.append((poly(x[-i])-poly(x[-(i+1)]))/(x[-i]-x[-(i+1)]))
        
            if sum(grad_polynomial) > 0:
                print("Water Level Increasing at a Rate:", sum(grad_polynomial))
            elif sum(grad_polynomial) < 0:
                print("Water Level Decreasing at a Rate:", sum(grad_polynomial))
            
            
       
        