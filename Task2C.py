""" Task 2C demonstration program """

from floodsystem import stationdata
from floodsystem import flood

stations = build_station_list()
update_water_levels(stations)

for station in stations_highest_rel_level(stations, 10):
    print(station[0].name, station[1])