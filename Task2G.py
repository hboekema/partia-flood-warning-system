
import floodsystem.flood
import matplotlib
from floodsystem import analysis
from floodsystem import stationdata
from floodsystem import plot
from floodsystem import datafetcher


stations = build_station_list()  #build station list
dt = 2

update_water_levels(stations)   #get latest data

stations_list = stations_level_over_threshold(stations,0.3)

#build lists to put the stations in depending on their current water level
stations_moderate = []
stations_high = []
stations_severe = []

for station in stations_list:  #categorise stations according to water level
    if station[1] > 2:
        stations_severe.append(station)
    elif station[1] > 1:
        stations_high.append(station)
    elif station[1] > 0.6:
        stations_moderate.append(station)
    else:
        stations_low.append(station)

for station in stations_severe:  #plot data for a particular list of stations 
                                 #and determine the rate at which water level is changing
    dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(days=dt))
    plot_water_level_with_fit(station[0], dates, levels, 4, True)
    
    
    
