""" Task1C demonstration program """

from floodsystem import geo

# Set initial conditions
stations = build_station_list()
p =  (52.2053, 0.1218)

station_list = []
for station in stations_within_radius(stations, p, 10):
    # Save just the names of the entries in the list returned by station_within_radius.
    # These stations are already in alphabetical order
      station_list.append(station.name)
      
print(station_list)