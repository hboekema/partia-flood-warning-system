from floodsystem import stationdata
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level


stations = build_station_list()  # build a list of stations
dt = 10 # use data for the past 10 days

update_water_levels(stations)

# get water level data for stations
for station_tuple in stations_highest_rel_level(stations, 5):
    dates, levels = fetch_measure_levels(station_tuple[0].measure_id, datetime.timedelta(days=dt))
    
    # plot graphs
    plot_water_levels(station_tuple[0], dates, levels)