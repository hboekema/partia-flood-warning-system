# Geo.py test file

import pytest
from floodsystem.geo import rivers_with_station, rivers_by_station_number, stations_within_radius
from floodsystem.stationdata import build_station_list
    

def test_rivers_with_station():
    """Test if the rivers with stations are identified""" 
    stations = build_station_list()
    river_stations = rivers_with_station(stations)
    assert len(river_stations) > 0

def test_rivers_by_station_number():
    """Test if the station numbers are identified correctly"""
    stations = build_station_list()
    # A list containing just the Thames should be returned - we know the data for this
    Thames = rivers_by_station_number(stations, 1)
    assert Thames[0] == ("Thames", 55)
    
def test_stations_within_radius():
    """Test whether the it identifies that there are any stations at all"""
    stations = build_station_list()
    stations_okay = stations_within_radius(stations, (0, 0), 1e20)
    assert len(stations_okay) > 0