""" Task1B demonstration program """

from floodsystem import geo
from floodsystem.stationdata import build_station_list


stations = build_station_list()
p =  (52.2053, 0.1218)


all_stations = stations_by_distance(stations, p)      #get all stations and their distances from point p

sort_stations = sorted(all_stations, key=lambda x: x[1])    #sort the stat

first_10_stations = sort_stations[:10]

last_10_stations = sort_stations[-10:-1]

print(first_10_stations)
print(last_10_stations)

    
