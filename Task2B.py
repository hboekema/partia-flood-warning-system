""" Task 2B demonstration program """

from floodsystem.flood import stations_level_over_threshold
from floodsystem import stationdata

station_list = build_station_list()

# Update water levels and subsequently check and save the stations with a
# relative water level above 0.8
update_water_levels(station_list)
checked_stations = stations_level_over_threshold(station_list, 0.8)
for station in checked_stations:
    print(station[0].name, station[1], '\n')